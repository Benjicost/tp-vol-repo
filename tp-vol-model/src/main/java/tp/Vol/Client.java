package tp.Vol;

import java.util.ArrayList;

public abstract class Client extends Personne {
	
	private String moyenPaiement;
	private Long Id;	
	private Adresse facturation;
	private Adresse principale;
//	private Civilite civilite;
//	private String nom;
//	private String prenoms;
//	private String nomEntreprise;
//	private String numTVA;
//	private String typeEntreprise;
//	private int numeroSIRET;
	
	private ArrayList<Passager> reservations = new ArrayList<Passager>();


	public String getMoyenPaiement() {
		return moyenPaiement;
	}

	public void setMoyenPaiement(String moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}

	public Adresse getFacturation() {
		return facturation;
	}

	public void setFacturation(Adresse facturation) {
		this.facturation = facturation;
	}

	public Adresse getPrincipale() {
		return principale;
	}

	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

	public ArrayList<Passager> getReservations() {
		return reservations;
	}

	public void setReservations(ArrayList<Passager> reservations) {
		this.reservations = reservations;
	}
	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}
	
	

	
//	public Civilite getCivilite() {
//		return civilite;
//	}
//
//	public void setCivilite(Civilite civilite) {
//		this.civilite = civilite;
//	}
//
//	public String getNom() {
//		return nom;
//	}
//
//	public void setNom(String nom) {
//		this.nom = nom;
//	}
//
//	public String getPrenoms() {
//		return prenoms;
//	}
//
//	public void setPrenoms(String prenoms) {
//		this.prenoms = prenoms;
//	}
//
//	public String getNomEntreprise() {
//		return nomEntreprise;
//	}
//
//	public void setNomEntreprise(String nomEntreprise) {
//		this.nomEntreprise = nomEntreprise;
//	}
//
//	public String getNumTVA() {
//		return numTVA;
//	}
//
//	public void setNumTVA(String numTVA) {
//		this.numTVA = numTVA;
//	}
//
//	public String getTypeEntreprise() {
//		return typeEntreprise;
//	}
//
//	public void setTypeEntreprise(String typeEntreprise) {
//		this.typeEntreprise = typeEntreprise;
//	}
//
//	public int getNumeroSIRET() {
//		return numeroSIRET;
//	}
//
//	public void setNumeroSIRET(int numeroSIRET) {
//		this.numeroSIRET = numeroSIRET;
//	}

	public Client() {
		super();
	}
	
	
	

}
