package tp.Vol;

import java.util.ArrayList;

public class Voyage {
	
	private Long voyageId;
	
	private ArrayList<Reservation> reservations = new ArrayList<Reservation>();
	
	private ArrayList<VoyageVol> voyagevols = new ArrayList<VoyageVol>();

	public ArrayList<Reservation> getReservations() {
		return reservations;
	}

	public void setReservations(ArrayList<Reservation> reservations) {
		this.reservations = reservations;
	}

	public ArrayList<VoyageVol> getVolvoyages() {
		return voyagevols;
	}

	public void setVolvoyages(ArrayList<VoyageVol> voyagevols) {
		this.voyagevols = voyagevols;
	}

	public Voyage() {
		super();
	}

	public Long getVoyageId() {
		return voyageId;
	}

	public void setVoyageId(Long voyageId) {
		this.voyageId = voyageId;
	}
	
	

}
