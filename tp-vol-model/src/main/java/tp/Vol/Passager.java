package tp.Vol;

import java.util.ArrayList;
import java.util.Date;

public class Passager extends Personne {
	
	private Long id_passager;
	private Civilite civilite;
	private String nom;
	private String prenoms;
	private String numIdentite;
	private String nationalite;
	private String typePI;
	private Date dateValiditePI;
	private Date dtNaissance;
	private String mail;
	private String telephone;
	private Long id_adresse_principale;

	private ArrayList<Passager> reservations = new ArrayList<Passager>();
	
	
	public Long getId_passager() {
		return id_passager;
	}
	public void setId_passager(Long id_passager) {
		this.id_passager = id_passager;
	}
	
	public Civilite getCivilite() {
		return civilite;
	}
	public void setCivilite(Civilite civilite) {
		this.civilite = civilite;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenoms() {
		return prenoms;
	}
	public void setPrenoms(String prenoms) {
		this.prenoms = prenoms;
	}
	public String getNumIdentite() {
		return numIdentite;
	}
	public void setNumIdentite(String numIdentite) {
		this.numIdentite = numIdentite;
	}
	public String getNationalite() {
		return nationalite;
	}
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	public String getTypePI() {
		return typePI;
	}
	public void setTypePI(String typePI) {
		this.typePI = typePI;
	}
	public Date getDateValiditePI() {
		return dateValiditePI;
	}
	public void setDateValiditePI(Date dateValiditePI) {
		this.dateValiditePI = dateValiditePI;
	}
	public Date getDtNaissance() {
		return dtNaissance;
	}
	public void setDtNaissance(Date dtNaissance) {
		this.dtNaissance = dtNaissance;
	}
	public ArrayList<Passager> getReservations() {
		return reservations;
	}
	public void setReservations(ArrayList<Passager> reservations) {
		this.reservations = reservations;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Long getId_adresse_principale() {
		return id_adresse_principale;
	}
	public void setId_adresse_principale(Long id_adresse_principale) {
		this.id_adresse_principale = id_adresse_principale;
	}
	public Passager() {
		super();
	}
	public Passager(Civilite civilite, String nom, String prenoms, String numIdentite, String nationalite, String typePI,
			Date dateValiditePI, Date dtNaissance, ArrayList<Passager> reservations, String mail, String telephone, Long id_adresse_principale) {
		super();
		this.civilite = civilite;
		this.nom = nom;
		this.prenoms = prenoms;
		this.numIdentite = numIdentite;
		this.nationalite = nationalite;
		this.typePI = typePI;
		this.dateValiditePI = dateValiditePI;
		this.dtNaissance = dtNaissance;
		this.reservations = reservations;
		this.mail = mail;
		this.telephone = telephone;
		this.id_adresse_principale = id_adresse_principale;
	}



}
