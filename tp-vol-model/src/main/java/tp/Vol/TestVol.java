package tp.Vol;

import java.util.Date;

public class TestVol {

	private static final Boolean True = null;

	public static void main(String[] args) {

//		ClientParticulier client1 = new ClientParticulier();
//		client1.setPrenoms("Benjamin");
//		client1.setNom("Costille");
//		client1.setCivilite("M.");
//		
//		System.out.println("Client "+client1.getCivilite()+" "+client1.getPrenoms()+" "+client1.getNom());
//		
//		ClientPro client2 = new ClientPro();
//		client2.setNomEntreprise("Sopra");
//		client2.setNumeroSIRET(587455145);
//		client2.setNumTVA("20");
//		client2.setTypeEntreprise("Edition");
//
//		System.out.println("Client pro :"+client2.getNomEntreprise()+" "+client2.getNumeroSIRET()+" "+client2.getNumTVA()+" "+client2.getTypeEntreprise());
//		
//		Adresse adresse1 = new Adresse();
//		adresse1.setVoie("89 rue J.K. Kennedy");
//		adresse1.setComplement("chez AJC");
//		adresse1.setCodePostal("33000");
//		adresse1.setVille("Merignac");
//		adresse1.setPays("France");
//		
//		System.out.println(adresse1.getVoie()+" "+adresse1.getComplement()+" "+adresse1.getCodePostal()+" "+adresse1.getVille()+" "+adresse1.getPays());
//		
//		Ville besancon = new Ville();
//		besancon.setNom("Besan�on");
//		Ville bordeaux = new Ville();
//		bordeaux.setNom("Bordeaux");
//		Ville paris = new Ville();
//		bordeaux.setNom("Paris");
//
//		System.out.println(besancon.getNom());
//		
//		Aeroport bdo = new Aeroport();
//		bdo.setCode("BOD");
//		Aeroport bsc = new Aeroport();
//		bsc.setCode("BSC");
//		Aeroport cdg = new Aeroport();
//		bdo.setCode("CDG");
//		
//		System.out.println(bdo.getCode());
//		
//		bdo.getVilles().add(bordeaux);
//		bsc.getVilles().add(besancon);
//		bsc.getVilles().add(paris);
//		
//		Vol besanconbordeaux = new Vol();
//		besanconbordeaux.setNbPlaces(100);
//		besanconbordeaux.setNumero(350);
//		besanconbordeaux.setDateDepart(new Date(2019,05,19));
//		besanconbordeaux.setDateArrivee(new Date(2019,05,19));
//		besanconbordeaux.setOuvert(True);
//		besanconbordeaux.setDepart(bsc);
//		besanconbordeaux.setArrivee(bdo);
//		
//		Vol bordeauxparis = new Vol();
//		bordeauxparis.setNbPlaces(101);
//		bordeauxparis.setNumero(300);
//		bordeauxparis.setDateDepart(new Date(2019,05,19));
//		bordeauxparis.setDateArrivee(new Date(2019,05,19));
//		bordeauxparis.setOuvert(True);
//		bordeauxparis.setDepart(bdo);
//		bordeauxparis.setArrivee(cdg);
//		
//		Passager pass1 = new Passager();
//		pass1.setCivilite("M.");
//		pass1.setDtNaissance(new Date(1991,01,11));
//		pass1.setMail("blablabla@bla.com");
//		pass1.setNationalite("Fran�aise");
//		pass1.setPrenoms("Benjamin");
//		pass1.setNom("Costille");
//		pass1.setTypePI("CNI");
//
//		System.out.println(pass1.getCivilite()+" "+pass1.getPrenoms()+" "+pass1.getNom()+" "+pass1.getNationalite()+" "+pass1.getMail());
//		
//		Reservation res1 = new Reservation();
//		res1.setNumero("5858");
//		res1.setTarif(0.99);
//		
//		res1.setPassager(pass1);
//		
//		Voyage voyage1 = new Voyage();
//		voyage1.getReservations().add(res1);
//		
//		VoyageVol vv1 = new VoyageVol();
//		vv1.setOrdre(1);
//		vv1.setVols(besanconbordeaux);
//		vv1.setVoyages(voyage1);
//		
//		
//		VoyageVol vv2 = new VoyageVol();
//		vv2.setOrdre(2);
//		vv2.setVols(bordeauxparis);
//		vv2.setVoyages(voyage1);
//		
//		Compagnie airfrance = new Compagnie();
//		airfrance.setNomCompagnie("Air France");
//
//		airfrance.getVols().add(bordeauxparis);
//		airfrance.getVols().add(besanconbordeaux);
//		
//		System.out.println("R�servation num�ro :" +res1.getNumero());
//		System.out.println("Trajet 1 : d�part "+ besancon.getNom()+ "desservi par l'a�roport de "+bsc.getVilles() + bsc.getCode());
//
//		
//	}
		
		
		Date dateDepartBdx = new Date(2019, 7, 10);
        Date dateArriveeAms = new Date(2019, 7, 11);
        Date dateDepartAms = new Date(2019, 7, 12);
        Date dateArriveeNrb = new Date(2019, 7, 13);

        String civilite = "Monsieur";
        String nom = "Costille";
        String prenoms = "Benjamin";
        Date dateReservation = new Date(2019, 7, 28);
        String numero = "RTF55";
        float tarif = 1500;

        Aeroport bdx = new Aeroport();
        bdx.setCode("BDX");

        Aeroport narb = new Aeroport();
        narb.setCode("NRB");

        Aeroport ams = new Aeroport();
        ams.setCode("NYK");

        System.out.println(bdx.getCode() + " - " + narb.getCode() + " - " + ams.getCode());

        Ville bodx = new Ville();
        bodx.setNom("Bordeaux");

        Ville nrb = new Ville();
        nrb.setNom("Narbonne");

        Ville amst = new Ville();
        amst.setNom("New York");

        System.out.println(bodx.getNom() + " - " + nrb.getNom() + " - " + amst.getNom());


        bdx.getVilles().add(bodx);
        narb.getVilles().add(nrb);
        ams.getVilles().add(amst);

 
        Compagnie af = new Compagnie();
        af.setNomCompagnie("AirFrance");
        
        System.out.println("Compagnie " + af.getNomCompagnie());


        Vol bdxnyk = new Vol();
        Vol nyknrb = new Vol();
        
        for(Ville toto : bdx.getVilles()) {
            System.out.println(toto.getNom());
        }
        
                    
        System.out.println("Trajet 1 : D�part le " + dateDepartBdx + " de l'a�roport de " + bdx.getVilles().get(0).getNom()
                + ", arriv�e le " + dateArriveeAms + " � l'a�roport de " + ams.getVilles().get(0).getNom());
        System.out.println("Trajet 2 : D�part le " + dateDepartAms + " de l'a�roport de " + ams.getVilles().get(0).getNom()
                + ", arriv�e le " + dateArriveeNrb + " � l'a�roport de " + narb.getVilles().get(0).getNom());
        System.out.println("D�part le " + dateDepartBdx + ", arriv�e le " + dateArriveeNrb + " � l'a�roport de "
                + narb.getVilles().get(0).getNom());

        
        
        af.getVols().add(bdxnyk);
        af.getVols().add(nyknrb);
        


        Voyage voyage = new Voyage();


        VoyageVol bordeauxamsterdam = new VoyageVol();
        VoyageVol amsterdamnairobi = new VoyageVol();

        bordeauxamsterdam.setVols(bdxnyk);
        amsterdamnairobi.setVols(nyknrb);
        
        bordeauxamsterdam.setVoyages(voyage);
        amsterdamnairobi.setVoyages(voyage);
        
        ClientParticulier benjamincostille = new ClientParticulier();
        benjamincostille.setNom(nom);
        benjamincostille.setPrenoms(prenoms);
        benjamincostille.setCivilite(civilite);
        
        Passager benjamin = new Passager(nom, prenoms, numero, numero, numero, numero, dateReservation, dateReservation, null);
        System.out.println("Passager " + benjamin.getNom() + " " + benjamin.getPrenoms());

        Reservation res = new Reservation();
        res.setPassager(benjamin);
        res.setVoyage(voyage);
        res.setStatut(true);
        res.setTarif(tarif);
        res.setNumero("855722");

        System.out.println("R�servation num�ro :" + res.getNumero() + " , tarif = " + tarif + " �");

        Adresse benjaminadresse = new Adresse();
        benjaminadresse.setCodePostal("25000");
        benjaminadresse.setPays("France");
        benjaminadresse.setVille("Besan�on");
        benjaminadresse.setVoie("88 rue machin");
        benjamin.setPrincipale(benjaminadresse);

        System.out.println("Adresse principale : " + benjamin.getPrincipale().getVoie() + " " + benjamin.getPrincipale().getCodePostal()+ " " +  benjamin.getPrincipale().getVille());


    }

}
