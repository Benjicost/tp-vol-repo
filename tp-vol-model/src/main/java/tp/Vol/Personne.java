package tp.Vol;

public abstract class Personne {
	
	private String mail;
	private String telephone;
	
	private Adresse principale;
	
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	public Adresse getPrincipale() {
		return principale;
	}
	public void setPrincipale(Adresse principale) {
		this.principale = principale;
	}

	
	

}
