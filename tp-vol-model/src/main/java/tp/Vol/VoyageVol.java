package tp.Vol;

public class VoyageVol {
	
	private int ordre;
	
	private Voyage voyages;
	private Vol vols;
	
	public int getOrdre() {
		return ordre;
	}
	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}
	public Voyage getVoyages() {
		return voyages;
	}
	public void setVoyages(Voyage voyages) {
		this.voyages = voyages;
	}
	public Vol getVols() {
		return vols;
	}
	public void setVols(Vol vols) {
		this.vols = vols;
	}
	
	public VoyageVol() {
		super();
	}
	
	

}
