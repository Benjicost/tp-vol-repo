package tp.Vol;

import java.util.Date;

public class Reservation {
	
	private String numero;
	private Boolean statut;
	private double tarif;
	private float tauxTVA;
	private Date dateReservation;
	
	private Passager passager;
	private Client client;
	private Voyage voyage;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Boolean getStatut() {
		return statut;
	}
	public void setStatut(Boolean statut) {
		this.statut = statut;
	}
	public double getTarif() {
		return tarif;
	}
	public void setTarif(double tarif) {
		this.tarif = tarif;
	}
	public float getTauxTVA() {
		return tauxTVA;
	}
	public void setTauxTVA(float tauxTVA) {
		this.tauxTVA = tauxTVA;
	}
	public Date getDateReservation() {
		return dateReservation;
	}
	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}
	public Passager getPassager() {
		return passager;
	}
	public void setPassager(Passager passager) {
		this.passager = passager;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Voyage getVoyage() {
		return voyage;
	}
	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}
	
	public Reservation() {
		super();
	}


}
