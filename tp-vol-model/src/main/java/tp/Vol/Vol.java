package tp.Vol;

import java.util.ArrayList;
import java.util.Date;

public class Vol {
	
	private Date dateDepart;
	private String numero;
	private Date dateArrivee;
	private Boolean ouvert;
	private int nbPlaces;
	
	private Aeroport arrivee;
	private Aeroport depart;
	private ArrayList<VoyageVol> voyagevols = new ArrayList<VoyageVol>();
	private Compagnie compagnie;
	
	public Date getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Date getDateArrivee() {
		return dateArrivee;
	}
	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}
	public Boolean getOuvert() {
		return ouvert;
	}
	public void setOuvert(Boolean ouvert) {
		this.ouvert = ouvert;
	}
	public int getNbPlaces() {
		return nbPlaces;
	}
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}
	public Aeroport getArrivee() {
		return arrivee;
	}
	public void setArrivee(Aeroport arrivee) {
		this.arrivee = arrivee;
	}
	public Aeroport getDepart() {
		return depart;
	}
	public void setDepart(Aeroport depart) {
		this.depart = depart;
	}
	public ArrayList<VoyageVol> getVoyagevols() {
		return voyagevols;
	}
	public void setVolvoyages(ArrayList<VoyageVol> voyagevols) {
		this.voyagevols = voyagevols;
	}
	public Compagnie getCompagnie() {
		return compagnie;
	}
	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}
	
	public Vol() {
		super();
	}
	
	
}
