package tp.Vol;

import java.util.ArrayList;

public class Compagnie {
	
	private String nomCompagnie;
	
	private ArrayList<Vol> vols = new ArrayList<Vol>();

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

	public ArrayList<Vol> getVols() {
		return vols;
	}

	public void setVols(ArrayList<Vol> vols) {
		this.vols = vols;
	}

	public Compagnie() {
		super();
	}

	public Compagnie(String nomCompagnie, ArrayList<Vol> vols) {
		super();
		this.nomCompagnie = nomCompagnie;
		this.vols = vols;
	}

}
