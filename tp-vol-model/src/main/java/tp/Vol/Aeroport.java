package tp.Vol;

import java.util.ArrayList;

public class Aeroport {
	
	private String code;
	
	private ArrayList<Ville> villes = new ArrayList<Ville>();
	
	private ArrayList<Vol> vols = new ArrayList<Vol>();

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public ArrayList<Ville> getVilles() {
		return villes;
	}

	public void setVilles(ArrayList<Ville> villes) {
		this.villes = villes;
	}

	public ArrayList<Vol> getVols() {
		return vols;
	}

	public void setVols(ArrayList<Vol> vols) {
		this.vols = vols;
	}

	public Aeroport() {
		super();
	}

}
