package tp.Vol;

import java.util.ArrayList;

public class Ville {

	private String nom;
	
	private ArrayList<Aeroport> aeroports = new ArrayList<Aeroport>();

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Aeroport> getAeroports() {
		return aeroports;
	}

	public void setAeroports(ArrayList<Aeroport> aeroports) {
		this.aeroports = aeroports;
	}

	public Ville() {
		super();
	}
	
}
