package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.Vol.Compagnie;
import tp.vol.Application;
import tp.vol.dao.ICompagnieDao;

public class CompagnieDaoSql implements ICompagnieDao {

	@Override
	public List<Compagnie> findAll() {
		
		List<Compagnie> compagnies = new ArrayList<Compagnie>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom_compagnie FROM compagnie");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				
				String nomCompagnie = rs.getString("nom_compagnie");
				
				Compagnie compagnie = new Compagnie();
				compagnie.setNomCompagnie(nomCompagnie);
				
				compagnies.add(compagnie);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return compagnies;
	}

	@Override
	public Compagnie findById(String id) {
		
		Compagnie compagnie = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom_compagnie FROM compagnie WHERE id = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String nomCompagnie = rs.getString("nom_compagnie");


				compagnie = new Compagnie();
				compagnie.setNomCompagnie(nomCompagnie);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return compagnie;
	}

	@Override
	public void create(Compagnie obj) {

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO compagnie (nom_compagnie) VALUES (?)");
			
			ps.setString(1, obj.getNomCompagnie());
	
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Compagnie obj) {

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE compagnie SET nom_compagnie WHERE nom_compagnie = ?");
			
			ps.setString(1, obj.getNomCompagnie());

			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Compagnie obj) {
		deleteById(obj.getNomCompagnie());
		
	}

	@Override
	public void deleteById(String id) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE compagnie WHERE nom_compagnie = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
