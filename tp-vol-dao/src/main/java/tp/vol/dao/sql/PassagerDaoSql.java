package tp.vol.dao.sql;


import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.Vol.Civilite;
import tp.Vol.Passager;
import tp.Vol.Reservation;
import tp.vol.Application;
import tp.vol.dao.IPassagerDao;

public class PassagerDaoSql implements IPassagerDao {

	@Override
	public List<Passager> findAll() {
		List<Passager> passagers = new ArrayList<Passager>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT INTO passager (id_passager, civilite, nom, prenom, num_identite, nationalite, type_PI,date_validitePI ,date_naissance,mail, telephone, id_adresse_principale) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long id_passager = rs.getLong("id_passager");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				String num_identite = rs.getString("num_identite");
				String nationalite = rs.getString("nationalite");
				String type_PI = rs.getString("type_PI");
				Date date_validitePI = rs.getDate("date_validitePI");
				Date date_naissance = rs.getDate("date_naissance");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				Long id_adresse_principale = rs.getLong("id_adresse_principale");
//				String reservations_num = rs.getString("reservations_num");

				Passager passager = new Passager();
				passager.setId_passager(id_passager);
				passager.setNom(nom);
				passager.setPrenoms(prenom);
				passager.setDtNaissance(date_naissance);
				passager.setNumIdentite(num_identite);
				passager.setNationalite(nationalite);
				passager.setCivilite(Civilite.valueOf(civilite));	
				passager.setTypePI(type_PI);
				passager.setDateValiditePI(date_validitePI);
				passager.setMail(mail);
				passager.setTelephone(telephone);
				passager.setId_adresse_principale(id_adresse_principale);



				//				Liens :
//
//				if (reservations_num != null) {
//					Reservation numero_reservation = Application.getInstance().getReservationDao().findById(reservations_num);
//					numero_reservation.setNumero(reservations_num);
//				}


				passagers.add(passager);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return passagers;
	}


	@Override
	public Passager findById(Long id) {
		Passager passager = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT civilite, nom, prenom, num_identite, nationalite, type_PI,date_validitePI ,date_naissance,mail, telephone, id_adresse_principale FROM passager WHERE id_passager = ?");

			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				Long id_passager = rs.getLong("id_passager");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenom = rs.getString("prenom");
				String num_identite = rs.getString("num_identite");
				String nationalite = rs.getString("nationalite");
				String type_PI = rs.getString("type_PI");
				Date date_validitePI = rs.getDate("date_validitePI");
				Date date_naissance = rs.getDate("date_naissance");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				Long id_adresse_principale = rs.getLong("id_adresse_principale");
				

				passager = new Passager();
				passager.setId_passager(id_passager);
				passager.setNom(nom);
				passager.setPrenoms(prenom);
				passager.setDtNaissance(date_naissance);
				passager.setNumIdentite(num_identite);
				passager.setNationalite(nationalite);
				passager.setCivilite(Civilite.valueOf(civilite));	
				passager.setTypePI(type_PI);
				passager.setDateValiditePI(date_validitePI);
				passager.setMail(mail);
				passager.setTelephone(telephone);
				passager.setId_adresse_principale(id_adresse_principale);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return passager;
	}	


	@Override
	public void create(Passager obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO passager (id_passager, civilite, nom, prenom, num_identite, nationalite, type_PI,date_validitePI ,date_naissance,mail, telephone, id_adresse_principale) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");

			ps.setLong(1, obj.getId_passager());
			ps.setString(2, obj.getCivilite().name());
			ps.setString(3, obj.getNom());
			ps.setString(4, obj.getPrenoms());
			ps.setString(5, obj.getNumIdentite());
			ps.setString(6,obj.getNationalite());
			ps.setString(7,obj.getTypePI());
			ps.setDate(8,new Date(obj.getDateValiditePI().getTime()));
			ps.setDate(9,new Date(obj.getDtNaissance().getTime()));
			ps.setString(10,obj.getMail());
			ps.setString(11,obj.getTelephone());
			ps.setLong(12,obj.getId_adresse_principale());


//			if(obj.getReservations() != null && obj.getReservations().getNumero() != null) {
//				ps.setLong(13, obj.getReservations().getId());
//			} else {
//				ps.setNull(13, Types.VARCHAR);
//			}
			
//			if(obj.getFormateur() != null && obj.getFormateur().getNumero() != null) {
//				ps.setLong(7, obj.getFormateur().getId());
//			} else {
//				ps.setNull(7, Types.VARCHAR);
//			}
			
//			if (reservations_num != null) {
//				Reservation numero_reservation = Application.getInstance().getReservationDao().findById(reservations_num);
//				numero_reservation.setNumero(reservations_num);
//			}


			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Passager obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE passager SET civilite = ?, nom = ?, prenom = ?, num_identite = ?, nationalite = ?, type_PI = ?, date_validitePI = ?, date_naissance = ? ,mail = ? , telephone = ? , id_adresse_principale = ? WHERE id_passager = ?");


			ps.setString(1, obj.getCivilite().name());
			ps.setString(2, obj.getNom());
			ps.setString(3, obj.getPrenoms());
			ps.setString(4, obj.getNumIdentite());
			ps.setString(5,obj.getNationalite());
			ps.setString(6,obj.getTypePI());
			ps.setDate(7,new Date(obj.getDateValiditePI().getTime()));
			ps.setDate(8,new Date(obj.getDtNaissance().getTime()));
			ps.setString(9,obj.getMail());
			ps.setString(10,obj.getTelephone());
			ps.setLong(11,obj.getId_adresse_principale());
//			ps.setLong(12, obj.getId_passager());

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(Passager obj) {
		deleteById(obj.getId_passager());
	}		


	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE passager WHERE id_passager = ?");

			ps.setLong(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}		
}


