package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.vol.Application;
import tp.Vol.Adresse;
import tp.vol.dao.IAdresseDao;

public class AdresseDaoSql implements IAdresseDao{

	@Override
	public List<Adresse> findAll() {
		List<Adresse> adresses = new ArrayList<Adresse>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT id_adresse, voie, complement, code_postal, ville, pays FROM adresse");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long idAdresse = rs.getLong("id_adresse");
				String voie = rs.getString("voie");
				String complement = rs.getString("complement");
				String codePostal = rs.getString("code_postal");
				String ville = rs.getString("ville");
				String pays = rs.getString("pays");

				Adresse adresse = new Adresse();
				adresse.setVoie(voie);
				adresse.setCodePostal(codePostal);;
				adresse.setIdAdresse(idAdresse);
				adresse.setComplement(complement);
				adresse.setCodePostal(codePostal);
				adresse.setVille(ville);
				adresse.setPays(pays);

				
				adresses.add(adresse);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return adresses;
	}


	@Override
	public Adresse findById(Long id) {

		Adresse adresse = null;
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT id_adresse, voie, complement, code_postal, ville, pays FROM adresse where id_adresse=?");

			ps.setLong(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String voie = rs.getString("voie");
				String complement = rs.getString("complement");
				String codePostal = rs.getString("code_postal");
				String ville = rs.getString("ville");
				String pays = rs.getString("pays");

				adresse = new Adresse();
				adresse.setVoie(voie);
				adresse.setCodePostal(codePostal);;
				adresse.setIdAdresse(id);
				adresse.setComplement(complement);
				adresse.setCodePostal(codePostal);
				adresse.setVille(ville);
				adresse.setPays(pays);
				
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return adresse;
		}

	@Override
	public void create(Adresse obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO adresse (id_adresse, voie, complement, code_postal, ville, pays) VALUES (?,?,?,?,?,?)");
			
			ps.setLong(1, obj.getIdAdresse());
			ps.setString(2, obj.getVoie());
			ps.setString(3, obj.getComplement());
			ps.setString(4, obj.getCodePostal());
			ps.setString(5, obj.getVille());
			ps.setString(6, obj.getPays());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}


	@Override
	public void update(Adresse obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE adresse SET voie = ?, complement = ?, code_postal = ?, ville = ?, pays = ? WHERE id_adresse = ?");
			
			ps.setString(1, obj.getVoie());
			ps.setString(2, obj.getComplement());
			ps.setString(3, obj.getCodePostal());
			ps.setString(4, obj.getVille());
			ps.setString(5, obj.getPays());
			
			
			ps.setLong(6, obj.getIdAdresse());

			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(Adresse obj) {
		deleteById(obj.getIdAdresse());
		
	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE adresse WHERE id = ?");

			ps.setLong(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
