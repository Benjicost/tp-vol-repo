package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.Vol.Passager;
import tp.Vol.Reservation;
import tp.Vol.Voyage;
import tp.vol.Application;
import tp.vol.dao.IVoyageDao;

public class VoyageDaoSql implements IVoyageDao {

	@Override
	public List<Voyage> findAll() {
		
		List<Voyage> voyages = new ArrayList<Voyage>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT numero_reservation, ordre_voyage_vol, voyage_id FROM voyage");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				
				Long voyageId = rs.getLong("voyage_id");
				
				Voyage voyage = new Voyage();
				voyage.setVoyageId(voyageId);


				
				voyages.add(voyage);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return voyages;

	}

	@Override
	public Voyage findById(Long id) {
		
		Voyage voyage = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT numero, date_depart, date_arrivee, ouvert, nombre_places, aer_depart_code, aer_arrivee_code, nom_compagnie FROM vol WHERE numero = ?");

			ps.setLong(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {

				Long voyageId = rs.getLong("voyage_id");	
				voyage.setVoyageId(voyageId);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return voyage;

	}

	@Override
	public void create(Voyage obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO vol (, , voyage_id) VALUES (?,?,?)");
			


			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void update(Voyage obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE voyage SET numero_reservation = ?, ordre_voyage_vol = ?, voyage_id = ? WHERE voyage_id = ?");
			

			ps.setLong(3, obj.getVoyageId());
					
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Voyage obj) {
		
		deleteById(obj.getVoyageId());
	}

	@Override
	public void deleteById(Long id) {
		
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE voyage WHERE voyage_id = ?");

			ps.setLong(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
}


