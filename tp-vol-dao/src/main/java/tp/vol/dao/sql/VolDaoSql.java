package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.Vol.Aeroport;
import tp.Vol.Passager;
import tp.Vol.Vol;
import tp.vol.Application;
import tp.vol.dao.IVolDao;

public class VolDaoSql implements IVolDao{

	@Override
	public List<Vol> findAll() {
		
		List<Vol> vols = new ArrayList<Vol>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT numero, date_depart, date_arrivee, ouvert, nombre_places, aer_depart_code, aer_arrivee_code, nom_compagnie FROM vol");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				
				String numero = rs.getString("numero");
				Date dateDepart = rs.getDate("date_depart");
				Date dateArrivee = rs.getDate("date_arrivee");
				Boolean ouvert = rs.getBoolean("ouvert");
				Integer nbPlaces = rs.getInt("nombre_places");
				String depart = rs.getString("aer_depart_code");
				String arrivee = rs.getString("aer_arrivee_code");
				String compagnie = rs.getString("compagnie");
				
				Vol vol = new Vol();
				vol.setNumero(numero);
				vol.setDateDepart(dateDepart);
				vol.setDateArrivee(dateArrivee);
				vol.setOuvert(ouvert);
				vol.setNbPlaces(nbPlaces);		
				
				
				vols.add(vol);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return vols;

	}

	@Override
	public Vol findById(String id) {
		
		Vol vol = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT numero, date_depart, date_arrivee, ouvert, nombre_places, aer_depart_code, aer_arrivee_code, nom_compagnie FROM vol WHERE numero = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String numero = rs.getString("numero");
				Date dateDepart = rs.getDate("date_depart");
				Date dateArrivee = rs.getDate("date_arrivee");
				Boolean ouvert = rs.getBoolean("ouvert");
				Integer nbPlaces = rs.getInt("nombre_places");
//				String depart = rs.getString("aer_depart_code");
//				String arrivee = rs.getString("aer_arrivee_code");
//				String compagnie = rs.getString("compagnie");
				
				vol.setNumero(numero);
				vol.setDateDepart(dateDepart);
				vol.setDateArrivee(dateArrivee);
				vol.setOuvert(ouvert);
				vol.setNbPlaces(nbPlaces);
//				vol.setDepart(depart);
//				vol.setArrivee(arrivee);
//				vol.setCompagnie(compagnie);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return vol;

	}

	@Override
	public void create(Vol obj) {	
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection
					.prepareStatement("INSERT INTO vol (numero, date_depart, date_arrivee, ouvert, nombre_places, aer_depart_code, aer_arrivee_code, nom_compagnie) VALUES (?,?,?,?,?,?,?,?)");

			ps.setString(1, obj.getNumero());
			ps.setDate(2, new Date(obj.getDateDepart().getTime()));
			ps.setDate(3, new Date(obj.getDateArrivee().getTime()));
			ps.setBoolean(4, obj.getOuvert());
			ps.setInt(5, obj.getNbPlaces());
			ps.setString(6, obj.getDepart().toString());
			ps.setString(7, obj.getArrivee().toString());
			ps.setString(8, obj.getCompagnie().toString());
			


			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void update(Vol obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE vol SET date_depart = ?, date_arrivee = ?, ouvert = ?, nombre_places = ?, aer_depart_code = ?, aer_arrivee_code = ?, nom_compagnie = ? WHERE numero = ?");
			

			
			ps.setDate(1, new Date(obj.getDateDepart().getTime()));
			ps.setDate(2, new Date(obj.getDateArrivee().getTime()));
			ps.setBoolean(3, obj.getOuvert());
			ps.setInt(4, obj.getNbPlaces());
			ps.setString(5, obj.getDepart().toString());
//			ps.setString(6, obj.getArrivee().toString());
//			ps.setString(7, obj.getCompagnie().toString());
//			ps.setString(8, obj.getNumero());
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Vol obj) {
		deleteById(obj.getNumero());
		
	}

	@Override
	public void deleteById(String id) {

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE vol WHERE numero = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
