package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.Vol.Client;
import tp.Vol.Passager;
import tp.Vol.Reservation;
import tp.vol.Application;
import tp.vol.dao.IReservationDao;

public class ReservationDaoSql implements IReservationDao{

	@Override
	public List<Reservation> findAll() {
		List<Reservation> reservations = new ArrayList<Reservation>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT INTO reservation (numero_reservation, statut, tarif, taux_tva, date_reservation) VALUES (?,?,?,?,?)");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {

				String numero_reservation = rs.getString("numero_reservation");
				Boolean statut = rs.getBoolean("statut");
				Float tarif = rs.getFloat("tarif");
				Float taux_tva = rs.getFloat("taux_tva");
				Date date_reservation = rs.getDate("date_reservation");
//				Long id_client = rs.getLong("id_client");
//				Long id_passager = rs.getLong("id_passager");


				Reservation reservation = new Reservation();
				reservation.setNumero(numero_reservation);
				reservation.setStatut(statut);
				reservation.setTarif(tarif);
				reservation.setTauxTVA(taux_tva);
				reservation.setDateReservation(date_reservation);


				//				Liens :

//				if (id_client != null) {
//					Client client = Application.getInstance().getClientDao().findById(id_client);
//					client.setId(id_client);
//				}
//
//				if (id_passager != null) {
//					Passager passager = Application.getInstance().getPassagerDao().findById(id_passager);
//					passager.setId_passager(id_passager);
//				}


				reservations.add(reservation);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return reservations;
	}


	@Override
	public Reservation findById(String id) {
		Reservation reservation = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT statut, tarif, taux_tva, date_reservation, FROM reservation WHERE numero_reservation = ?");

			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String numero_reservation = rs.getString("numero_reservation");
				Boolean statut = rs.getBoolean("statut");
				Double tarif = rs.getDouble("tarif");
				Float taux_tva = rs.getFloat("taux_tva");
				Date date_reservation = rs.getDate("date_reservation");
				Long id_client = rs.getLong("id_client");
				Long id_passager = rs.getLong("id_passager");


				reservation = new Reservation();
				reservation.setNumero(numero_reservation);
				reservation.setStatut(statut);
				reservation.setTarif(tarif);
				reservation.setTauxTVA(taux_tva);
				reservation.setDateReservation(date_reservation);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return reservation;
	}

	@Override
	public void create(Reservation obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO reservation (numero_reservation, statut, tarif, taux_tva, date_reservation) VALUES (?,?,?,?,?)");

			ps.setString(1, obj.getNumero());
			ps.setBoolean(2, obj.getStatut());
			ps.setDouble(3, obj.getTarif());
			ps.setFloat(4, obj.getTauxTVA());
			ps.setDate(5,new Date(obj.getDateReservation().getTime()));

//			if(obj.getClient() != null && obj.getClient().getId() != null) {
//				ps.setLong(6, obj.getClient().getId());
//			} else {
//				ps.setNull(6, Types.VARCHAR);
//			}
//
//			if(obj.getPassager() != null && obj.getPassager().getId_passager() != null) {
//				ps.setLong(7, obj.getPassager().getId_passager());
//			} else {
//				ps.setNull(7, Types.VARCHAR);
//			}



			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void update(Reservation obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE reservation SET statut = ?, tarif = ?, taux_tva = ?, date_reservation = ? WHERE numero_reservation = ?");

			ps.setBoolean(1, obj.getStatut());
			ps.setDouble(2, obj.getTarif());
			ps.setFloat(3, obj.getTauxTVA());
			ps.setDate(4, new Date(obj.getDateReservation().getTime()));
			ps.setLong(5,obj.getClient().getId());
			ps.setLong(6,obj.getPassager().getId_passager());


			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void delete(Reservation obj) {
		deleteById(obj.getNumero());

	}

	@Override
	public void deleteById(String id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE reservation WHERE numero_reservation = ?");

			ps.setString(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
