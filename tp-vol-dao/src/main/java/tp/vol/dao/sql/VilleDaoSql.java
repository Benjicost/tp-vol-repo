package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tp.Vol.Aeroport;
import tp.Vol.Compagnie;
import tp.Vol.Ville;
import tp.vol.Application;
import tp.vol.dao.IVilleDao;

public class VilleDaoSql implements IVilleDao{

	@Override
	public List<Ville> findAll() {
		
		List<Ville> villes = new ArrayList<Ville>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom FROM ville");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				
				String nom = rs.getString("nom");
				
				Ville ville = new Ville();
				ville.setNom(nom);
				
				villes.add(ville);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return villes;
	}


	@Override
	public Ville findById(String id) {
		
		
		Ville ville = null;
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("SELECT nom FROM ville WHERE nom = ?");

			ps.setString(1, id);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String nom = rs.getString("nom");

				ville = new Ville();
				ville.setNom(nom);

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		return ville;
	}

	@Override
	public void create(Ville obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("INSERT INTO ville (nom) VALUES (?)");
			
			ps.setString(1, obj.getNom());
	
			
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(Ville obj) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("UPDATE ville SET nom WHERE nom = ?");
			
			ps.setString(1, obj.getNom());

			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

	@Override
	public void delete(Ville obj) {
		deleteById(obj.getNom());

		
	}

	@Override
	public void deleteById(String id) {
		
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE ville WHERE nom = ?");

			ps.setString(1, id);
			
			int rows = ps.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}

}
