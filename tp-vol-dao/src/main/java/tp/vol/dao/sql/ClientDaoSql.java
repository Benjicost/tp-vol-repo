package tp.vol.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import tp.Vol.Adresse;
import tp.Vol.Civilite;
import tp.Vol.Client;
import tp.Vol.ClientParticulier;
import tp.Vol.ClientPro;
import tp.vol.Application;
import tp.vol.dao.IClientDao;

public class ClientDaoSql implements IClientDao {

	@Override
	public List<tp.Vol.Client> findAll() {
		List<Client> clients = new ArrayList<Client>();

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT id_client, moyen_paiement, type_client, mail, telephone, civilite, nom, prenoms, nom_entreprise, numero_siret, num_tva, type_entreprise, id_adresse_facturation, id_adresse_principale FROM client");

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				Long idClient = rs.getLong("id_client");
				String moyenPaiement = rs.getString("moyen_paiement");
				String typeClient = rs.getString("type_client");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenoms = rs.getString("prenoms");
				String nomEntreprise = rs.getString("nom_entreprise");
				int numeroSIRET = rs.getInt("numero_siret");
				String numTVA = rs.getString("num_tva");
				String typeEntreprise = rs.getString("type_entreprise");
				Long idAdresseFacturation = rs.getLong("id_adresse_facturation");
				Long idAdressePrincipale = rs.getLong("id_adresse_principale");

				if (typeClient.equals("ClientPro")) {
					ClientPro client = new ClientPro();
					client.setId(idClient);
					client.setMoyenPaiement(moyenPaiement);
					client.setMail(mail);
					client.setTelephone(telephone);
					client.setNomEntreprise(nomEntreprise);
					client.setNumeroSIRET(numeroSIRET);
					client.setNumTVA(numTVA);
					client.setTypeEntreprise(typeEntreprise);

					if (idAdresseFacturation != null) {
						Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdresseFacturation);
						client.setFacturation(adresse);
					}
					if (idAdressePrincipale != null) {
						Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdressePrincipale);
						client.setFacturation(adresse);
					}

					clients.add(client);
				}

				if (typeClient.equals("ClientParticulier")) {
					ClientParticulier client = new ClientParticulier();
					client.setId(idClient);
					client.setMoyenPaiement(moyenPaiement);
					client.setMail(mail);
					client.setTelephone(telephone);
					client.setCivilite(Civilite.valueOf(civilite));
					client.setNom(nom);
					client.setPrenoms(prenoms);

					if (idAdresseFacturation != null) {
						Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdresseFacturation);
						client.setFacturation(adresse);
					}
					if (idAdressePrincipale != null) {
						Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdressePrincipale);
						client.setFacturation(adresse);
					}

					clients.add(client);
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return clients;
	}

	@Override
	public tp.Vol.Client findById(Long id) {
		Client client = null;

		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"SELECT id_client, moyen_paiement, type_client, mail, telephone, civilite, nom, prenoms, nom_entreprise, numero_siret, num_tva, type_entreprise, id_adresse_facturation, id_adresse_principale FROM client WHERE id_client = ?");

			ps.setLong(1, id);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				String moyenPaiement = rs.getString("moyen_paiement");
				String typeClient = rs.getString("type_client");
				String mail = rs.getString("mail");
				String telephone = rs.getString("telephone");
				String civilite = rs.getString("civilite");
				String nom = rs.getString("nom");
				String prenoms = rs.getString("prenoms");
				String nomEntreprise = rs.getString("nom_entreprise");
				int numeroSIRET = rs.getInt("numero_siret");
				String numTVA = rs.getString("num_tva");
				String typeEntreprise = rs.getString("type_entreprise");
				Long idAdresseFacturation = rs.getLong("id_adresse_facturation");
				Long idAdressePrincipale = rs.getLong("id_adresse_principale");

				if (typeClient.equals("ClientPro")) {
					ClientPro clientPro = new ClientPro();
					clientPro.setId(id);
					clientPro.setMoyenPaiement(moyenPaiement);
					clientPro.setMail(mail);
					clientPro.setTelephone(telephone);
					clientPro.setNomEntreprise(nomEntreprise);
					clientPro.setNumeroSIRET(numeroSIRET);
					clientPro.setNumTVA(numTVA);
					clientPro.setTypeEntreprise(typeEntreprise);

					if (idAdresseFacturation != null) {
						Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdresseFacturation);
						clientPro.setFacturation(adresse);
					}
					if (idAdressePrincipale != null) {
						Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdressePrincipale);
						clientPro.setFacturation(adresse);
					}
					client = clientPro;
				}

				if (typeClient.equals("ClientParticulier")) {
					ClientParticulier clientParticulier = new ClientParticulier();
					clientParticulier.setId(id);
					clientParticulier.setMoyenPaiement(moyenPaiement);
					clientParticulier.setMail(mail);
					clientParticulier.setTelephone(telephone);
					clientParticulier.setCivilite(Civilite.valueOf(civilite));
					clientParticulier.setNom(nom);
					clientParticulier.setPrenoms(prenoms);

					if (idAdresseFacturation != null) {
						Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdresseFacturation);
						clientParticulier.setFacturation(adresse);
					}
					if (idAdressePrincipale != null) {
						Adresse adresse = Application.getInstance().getAdresseDao().findById(idAdressePrincipale);
						clientParticulier.setFacturation(adresse);
					}
					client = clientParticulier;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		return client;

	}

	@Override
	public void create(Client obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"INSERT INTO client (id_client, moyen_paiement, type_client, mail, telephone, civilite, nom, prenoms, nom_entreprise, numero_siret, num_tva, type_entreprise, id_adresse_facturation, id_adresse_principale) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

			ps.setLong(1, obj.getId());
			ps.setString(2, obj.getMoyenPaiement());
			ps.setString(4, obj.getMail());
			ps.setString(5, obj.getTelephone());

			if (obj instanceof ClientParticulier) {
				ClientParticulier client = (ClientParticulier) obj;

				ps.setString(3, "ClientParticulier");
				ps.setString(6, client.getCivilite().toString());
				ps.setString(7, client.getNom());
				ps.setString(8, client.getPrenoms());
				ps.setString(9, null);
				ps.setNull(10, Types.NUMERIC);
				ps.setString(11, null);
				ps.setString(12, null);
			}

			if (obj instanceof ClientPro) {
				ClientPro client = (ClientPro) obj;

				ps.setString(3, "ClientPro");
				ps.setString(6, null);
				ps.setString(7, null);
				ps.setString(8, null);
				ps.setString(9, client.getNomEntreprise());
				ps.setLong(10, client.getNumeroSIRET());
				ps.setString(11, client.getNumTVA());
				ps.setString(12, client.getTypeEntreprise());
			}

			if (obj.getFacturation() != null && obj.getFacturation().getIdAdresse() != null) {
				ps.setLong(13, obj.getFacturation().getIdAdresse());
			} else {
				ps.setNull(13, Types.NUMERIC);
			}
			if (obj.getPrincipale() != null && obj.getPrincipale().getIdAdresse() != null) {
				ps.setLong(14, obj.getPrincipale().getIdAdresse());
			} else {
				ps.setNull(14, Types.NUMERIC);
			}

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void update(tp.Vol.Client obj) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement(
					"UPDATE client SET moyen_paiement = ?, type_client = ?, mail = ?, telephone = ?, civilite = ?, nom = ?, prenoms = ?, nom_entreprise = ?, numero_siret = ?, num_tva = ?, type_entreprise = ?, id_adresse_facturation = ?, id_adresse_principale = ? WHERE id_client = ?");

			ps.setString(1, obj.getMoyenPaiement());
			ps.setString(3, obj.getMail());
			ps.setString(4, obj.getTelephone());

			if (obj instanceof ClientParticulier) {
				ClientParticulier client = (ClientParticulier) obj;

				ps.setString(2, "ClientParticulier");
				ps.setString(5, client.getCivilite().toString());
				ps.setString(6, client.getNom());
				ps.setString(7, client.getPrenoms());
				ps.setString(8, null);
				ps.setNull(9, Types.NUMERIC);
				ps.setString(10, null);
				ps.setString(11, null);
			}

			if (obj instanceof ClientPro) {
				ClientPro client = (ClientPro) obj;

				ps.setString(2, "ClientPro");
				ps.setString(5, null);
				ps.setString(6, null);
				ps.setString(7, null);
				ps.setString(8, client.getNomEntreprise());
				ps.setLong(9, client.getNumeroSIRET());
				ps.setString(10, client.getNumTVA());
				ps.setString(11, client.getTypeEntreprise());
			}

			if (obj.getFacturation() != null && obj.getFacturation().getIdAdresse() != null) {
				ps.setLong(12, obj.getFacturation().getIdAdresse());
			} else {
				ps.setNull(12, Types.NUMERIC);
			}
			if (obj.getPrincipale() != null && obj.getPrincipale().getIdAdresse() != null) {
				ps.setLong(13, obj.getPrincipale().getIdAdresse());
			} else {
				ps.setNull(13, Types.NUMERIC);
			}
			
			ps.setLong(14, obj.getId());
			
			
			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public void delete(tp.Vol.Client obj) {
		deleteById(obj.getId());

	}

	@Override
	public void deleteById(Long id) {
		Connection connection = null;
		try {
			connection = Application.getInstance().getConnection();

			PreparedStatement ps = connection.prepareStatement("DELETE eleve WHERE id = ?");

			ps.setLong(1, id);

			int rows = ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
