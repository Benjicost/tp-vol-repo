package src.test.java.tp.vol;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


import tp.Vol.Reservation;
import tp.vol.Application;
import tp.vol.dao.IReservationDao;

public class TestReservationDao {
	public static void main(String[] args) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");
		Reservation reservation = new Reservation();
		reservation.setNumero("blabla14");
		reservation.setStatut(true);
		reservation.setTarif(15.15D);
		reservation.setTauxTVA(0.20f);

		try {
			reservation.setDateReservation(sdf.parse("15-05-2019"));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		Application.getInstance().getReservationDao().create(reservation);
		System.out.println(reservation.getNumero());
		
	}
	}