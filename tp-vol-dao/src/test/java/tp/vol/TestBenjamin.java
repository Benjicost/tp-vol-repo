package tp.vol;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import tp.Vol.Aeroport;
import tp.Vol.Compagnie;
import tp.Vol.Ville;
import tp.Vol.Vol;
import tp.Vol.Voyage;

public class TestBenjamin {

	public static void main(String[] args) throws ParseException {
		
		Compagnie airtruc = new Compagnie();
		airtruc.setNomCompagnie("AirTruc");
		
		Ville perpignan = new Ville();
		perpignan.setNom("Perpignan");
		
		Aeroport prp = new Aeroport();
		prp.setCode("PRP");
		
		Aeroport bdx = new Aeroport();
		prp.setCode("PRP");
		
		Voyage perpiganbordeaux = new Voyage();
		perpiganbordeaux.setVoyageId(1455L);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy");

		Vol vol2 = new Vol();
		vol2.setNumero("F25");
		vol2.setDepart(prp);
		vol2.setArrivee(bdx);
		//vol2.setCompagnie(airtruc);
		vol2.setNbPlaces(152);
		vol2.setOuvert(true);
		vol2.setDateDepart(sdf.parse("02-11-2019"));
		vol2.setDateArrivee(sdf.parse("03-11-2019"));
		
		Application.getInstance().getCompagnieDao().create(airtruc);
		Application.getInstance().getVilleDao().create(perpignan);
		Application.getInstance().getAeroportDao().create(prp);
		
		Application.getInstance().getAeroportDao().delete(prp);
		Application.getInstance().getVilleDao().delete(perpignan);
		Application.getInstance().getCompagnieDao().delete(airtruc);
		Application.getInstance().getVolDao().create(vol2);

		

	}

}
